import React, { Component } from 'react'
import YTSearch from 'youtube-api-search';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail'
import Modal from 'react-modal';

const API_KEY = "AIzaSyAIK4grY2Huap-cyDRe5-myEGqb_1g2GFU";

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    transform             : 'translate(-50%, -50%)',
    width                 : '100%',
    padding               : '20px',
    background            : 'transparent',
    border                : 'none'
  }
};

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      videos: [],
      selectedVideo: null,
      modalIsOpen: false
    }

    this.videoSearch('Euro Fishing');
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  videoSearch(term) {
    YTSearch({key: API_KEY, term: term, maxResults: 6}, (data) => {
      console.log(data)
      this.setState({
        videos: data,
        selectedVideo: data[0]
      });
    });
  }
    
  openModal() {
    this.setState({modalIsOpen: true});
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  render() {
    return (
      <div className="container pt-4">
        <div className="row">
          <Modal
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.closeModal}
            style={customStyles}
          >
            <button onClick={this.closeModal} type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <VideoDetail video={this.state.selectedVideo}/>
          </Modal>
          <VideoList 
          onVideoSelect={userSelected => this.setState({selectedVideo: userSelected,modalIsOpen: true})}
          videos={this.state.videos}/>
        </div>
      </div>
    );
  }
}

export default App;

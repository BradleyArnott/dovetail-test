import React from 'react';

const VideoListItem = (props) => {
    const video = props.video;
    const onUserSelected = props.onUserSelected;

    const imageUrl = video.snippet.thumbnails.high.url;

    return ( 
        <li onClick={() => onUserSelected(video)} className="list-group-item">
            <div className="video-list media">
                <div className="media-body">
                    <img className="media-object img-fluid" src={imageUrl}/>
                    <span class="glyphicon glyphicon-play" aria-hidden="true"></span>

                    <div className="wrapper">
                        <div className="media-heading">{video.snippet.title}</div>
                    </div>
                </div>
            </div>
        </li>
    );
};

export default VideoListItem;
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Clone Repo

### `Git Clone - git clone https://BradleyArnott@bitbucket.org/BradleyArnott/dovetail-test.git`
### `cd dovetail-youtube`
### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### Note for install

Once all install pleased add the follow line to the params in the node module youtube-api-search

maxResults: options.maxResults
